package oppen.light.lib.luts

data class LUT (val name: String, val resId: Int, val adapterPosition: Int = -1)