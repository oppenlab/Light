package oppen.light.lib.luts

import android.content.Context
import android.graphics.Bitmap
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import oppen.light.lib.LutProcessor

class LUTFlow(context: Context) {

  val lutProc = LutProcessor(context)

  fun process(bitmap: Bitmap, luts: List<LUT>, onEmit: (lut: Pair<LUT, Bitmap>) -> Unit){
    CoroutineScope(Job() + Dispatchers.Default).launch {
      processAndEmit(bitmap, luts)
        .flowOn(Dispatchers.IO)
        .collect { readyLut ->
          onEmit(readyLut)
        }
    }
  }

  private fun processAndEmit(bitmap: Bitmap, luts: List<LUT>): Flow<Pair<LUT, Bitmap>> = flow {
    luts.forEach { lut ->
      val bitmap = lutProc.filter(bitmap, lut.resId)
      emit(Pair(lut, bitmap))
    }
  }
}