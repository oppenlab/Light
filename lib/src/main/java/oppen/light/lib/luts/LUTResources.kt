package oppen.light.lib.luts

import android.content.Context
import android.content.res.TypedArray

object LUTResources {

  fun getColourLuts(context: Context, brand: String): List<Pair<String, Int>>{
    return getLuts(context, "colour", brand)
  }

  fun getMonochromeLuts(context: Context, brand: String): List<Pair<String, Int>>{
    return getLuts(context, "monochrome", brand)
  }

  fun getLuts(context: Context, type: String, brand: String): List<Pair<String, Int>>{
   val key = "${type}_${brand.toLowerCase()}"
    val arrayId = context.resources.getIdentifier(key, "array", context.packageName)

    val lutIds: TypedArray = context.resources.obtainTypedArray(arrayId)

    val luts = arrayListOf<Pair<String, Int>>()

    for(index in 0 until lutIds.length()){
      val lutResId = lutIds.getResourceId(index, -1)
      if(lutResId != -1){

        val lutResName = context.resources.getResourceEntryName(lutResId)
        val label = lutResName!!.toLabel()
        println("LUT: brand: $brand type: $type, adding $label with id: $lutResName")
        luts.add(Pair(label, lutResId))
      }
    }

    lutIds.recycle()

    return luts
  }

  private fun String.toLabel(): String{
    val label = this.replace("_", " ")
    return label.split(" ").joinToString(" ") { it.capitalize() }
  }
}