package oppen.light.lib

import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsic3DLUT
import oppen.light.lib.filters.*

object RenderScriptProvider {

    lateinit var renderscript: RenderScript

    lateinit var rscriptLut: ScriptIntrinsic3DLUT
    lateinit var rscriptBrightness: ScriptC_brightness
    lateinit var rscriptSaturation: ScriptC_saturation
    lateinit var rscriptExposure: ScriptC_exposure
    lateinit var rscriptShadows: ScriptC_shadows
    lateinit var rscriptGrain: ScriptC_grain
    lateinit var rscriptContrast: ScriptC_contrast
    lateinit var rscriptVignette: ScriptC_vignette

    fun initialise(renderscript: RenderScript){
        this.renderscript = renderscript
        rscriptLut = ScriptIntrinsic3DLUT.create(renderscript, Element.U8_4(renderscript))
        rscriptBrightness = ScriptC_brightness(renderscript)
        rscriptSaturation = ScriptC_saturation(renderscript)
        rscriptExposure = ScriptC_exposure(renderscript)
        rscriptShadows = ScriptC_shadows(renderscript)
        rscriptGrain = ScriptC_grain(renderscript)
        rscriptContrast = ScriptC_contrast(renderscript)
        rscriptVignette = ScriptC_vignette(renderscript)
    }
}