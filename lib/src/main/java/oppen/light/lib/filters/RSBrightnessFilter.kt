package oppen.light.lib.filters

import android.renderscript.Allocation
import androidx.annotation.FloatRange
import oppen.light.lib.RenderScriptProvider

class RSBrightnessFilter: Filter.RenderScriptFilter() {

    private var rscriptBrightness: ScriptC_brightness = ScriptC_brightness(RenderScriptProvider.renderscript)

    @FloatRange(from=0.0,to=1.0)
    var brightness = 0.0f//-1.0 to 1

    override fun process(inAllocation: Allocation, outAllocation: Allocation) {
        rscriptBrightness._brightness = brightness
        rscriptBrightness.forEach_root(inAllocation, outAllocation)
    }
}