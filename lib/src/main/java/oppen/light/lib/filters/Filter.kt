package oppen.light.lib.filters

import android.graphics.Bitmap
import android.renderscript.Allocation

sealed class Filter {

    abstract val priority: Int

    abstract class RenderScriptFilter: Filter(){
        override val priority = 0
        abstract fun process(inAllocation: Allocation, outAllocation: Allocation)
    }
    abstract class BitmapFilter: Filter(){
        override val priority = 1
        abstract fun process(inBitmap: Bitmap): Bitmap
    }
}