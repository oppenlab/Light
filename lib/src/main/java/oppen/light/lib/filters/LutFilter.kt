package oppen.light.lib.filters

import android.content.Context
import android.graphics.BitmapFactory
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.Type
import oppen.light.lib.RenderScriptProvider

class LutFilter(
  val context: Context,
  private val resId: Int): Filter.RenderScriptFilter() {

  override fun process(inAllocation: Allocation, outAllocation: Allocation) {
    val lutBitmap = BitmapFactory.decodeResource(context.resources, resId)
    val width: Int = lutBitmap?.width ?: 0
    val height: Int = lutBitmap?.height ?: 0
    val sideLength = width / height

    val pixels = IntArray(width * height)
    val lut = IntArray(width * height)

    lutBitmap?.getPixels(pixels, 0, width, 0, 0, width, height)
    lutBitmap?.recycle()//Done with Lut bitmap

    var i = 0

    for (red in 0 until sideLength) {
      for (green in 0 until sideLength) {
        val p = red + green * width
        for (blue in 0 until sideLength) {
          lut[i++] = pixels[p + blue * height]
        }
      }
    }

    val renderScript = RenderScriptProvider.renderscript

    val type = Type.Builder(renderScript, Element.U8_4(renderScript))
      .setX(sideLength)
      .setY(sideLength)
      .setZ(sideLength)
      .create()

    val allocCube = Allocation.createTyped(renderScript, type)
    allocCube?.copyFromUnchecked(lut)
    RenderScriptProvider.rscriptLut.setLUT(allocCube)
    RenderScriptProvider.rscriptLut.forEach(inAllocation, outAllocation)
  }
}