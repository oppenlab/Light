package oppen.light.lib

import android.graphics.Bitmap
import android.renderscript.Allocation
import oppen.light.lib.filters.Filter

/**
 *
 * Filters are ordered by type:
 * RenderScript filters take precedence using the allocation to the RS kernel
 * Slower filters written in Kotlin (if any) that iterate the pixel array come after
 *
 */
class ImageProcessor {

    fun process(sourceBitmap: Bitmap?, filters: List<Filter>, onComplete: (bitmap: Bitmap?) -> Unit){

        if(sourceBitmap == null){
            println("ImageProc: sourceBitmap is null,  returning null")
            onComplete(null)
            return
        }

        val outputBitmap = Bitmap.createBitmap(sourceBitmap.width, sourceBitmap.height, sourceBitmap.config)
        val allocIn = Allocation.createFromBitmap(RenderScriptProvider.renderscript, sourceBitmap)
        val allocOut = Allocation.createFromBitmap(RenderScriptProvider.renderscript, outputBitmap)

        if(filters.isEmpty()){
            println("ImageProc: no filters returning null")
            onComplete(null)
            return
        }

        if(allocIn == null || allocOut == null){
            println("ImageProc: missing alloc null")
            onComplete(null)
            return
        }

        filters.sortedBy(Filter::priority).forEach { filter ->
            when (filter) {
                is Filter.RenderScriptFilter -> {
                    filter.process(allocIn!!, allocOut!!)
                    allocIn?.copyFrom(allocOut)
                }
                is Filter.BitmapFilter -> {
                    //The first time a BitmapFilter is encountered populate the mutable outputBitmap
                    when (outputBitmap.allocationByteCount) {
                        0 -> {
                            when {
                                filters.none { it is Filter.RenderScriptFilter } -> allocIn?.copyTo(outputBitmap)
                                else -> allocOut?.copyTo(outputBitmap)
                            }
                        }
                    }

                    //todo
                }
            }
        }

        allocOut.copyTo(outputBitmap)

        onComplete(outputBitmap)
    }
}