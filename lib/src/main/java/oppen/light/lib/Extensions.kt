package oppen.light.lib

import android.graphics.Bitmap
import oppen.light.lib.filters.Filter

fun Bitmap.filter(filters: List<Filter>, onComplete: (bitmap: Bitmap?) -> Unit){
    ImageProcessor().process(this, filters, onComplete)
}