package oppen.light

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import com.bumptech.glide.request.FutureTarget
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream

const val LIVE_PREVIEW_FILENAME = "live_preview.png"
const val LIVE_THUMBNAIL_SOURCE_FILENAME = "live_thumbnail_source.png"

class ImageIO(
    private val context: Context) {

    fun loadImage(uri: Uri, onLoad: (previewUri: Uri, thumbnailUri: Uri) -> Unit){
        println("Loading main preview image from: $uri")
        Glide.get(context).clearMemory()
        GlobalScope.launch {
            calculate(uri, context.resources.displayMetrics.widthPixels, LIVE_PREVIEW_FILENAME) { previewUri ->
                createThumbnail(previewUri) { thumbnailUri ->
                    Glide.get(context).clearDiskCache()
                    onLoad(previewUri, thumbnailUri)
                }
            }
        }
    }

    private fun createThumbnail(uri: Uri, onThumbnail: (thumbnailUri: Uri) -> Unit){
        calculate(uri, 120, LIVE_THUMBNAIL_SOURCE_FILENAME, onThumbnail)
    }

    /**
     *
     * Generates a smaller (if needed) image from the source image file.
     * @param uri Storage Access Framework content: uri - NOT a file: uri
     * @param width Max width of the preview image, likely the screen width
     * @param onComplete Lambda callback with resulting preview image Uri
     */
    private fun calculate(uri: Uri, width: Int, filename: String, onComplete: (uri: Uri) -> Unit){
        val bounds = BitmapFactory.Options()
        bounds.inJustDecodeBounds = true
        val inputStream = context.contentResolver.openInputStream(uri)
        BitmapFactory.decodeStream(inputStream, null, bounds)

        val sourceWidth = bounds.outWidth
        val sourceHeight = bounds.outHeight

        var previewWidth = sourceWidth
        var previewHeight = sourceHeight

        if (previewWidth > width) {
            val ratio = previewWidth / width
            previewWidth = width
            previewHeight /= ratio

            //Arbitrary value, only reduce image quality for (probably/possibly) full screen width images
            /*
                if(width > 800) {
                    previewWidth = (previewWidth / 1.5).toInt()
                    previewHeight = (previewHeight / 1.5).toInt()
                }
            */
        }

        generate(uri,previewWidth, previewHeight, filename){ outputUri ->
            onComplete(outputUri)
        }
    }

    private fun generate(source: Uri, targetWidth: Int, targetHeight: Int, targetFilename: String, onComplete: (uri: Uri) -> Unit){
        val sourceTarget: FutureTarget<Bitmap> = Glide.with(context)
            .asBitmap()
            .load(source)
            .submit(targetWidth, targetHeight)

        val sourceBitmap = sourceTarget.get()

        val outputFile = File(context.cacheDir, targetFilename)
        if (outputFile.exists()) {
            val deleted = outputFile.delete()
            println("$targetFilename already exists, deleted: $deleted")
        }

        FileOutputStream(outputFile).run {
            sourceBitmap.compress(Bitmap.CompressFormat.PNG, 90, this)
            close()
        }

        Glide.with(context).clear(sourceTarget)//Recycle

        onComplete(outputFile.toUri())
    }

    var futureTarget: FutureTarget<Bitmap>? = null

    fun get(uri: Uri, onLoaded: (bitmap: Bitmap?) -> Unit){
        GlobalScope.launch {
            futureTarget = Glide.with(context)
                .asBitmap()
                .load(uri)
                .submit()

            @Suppress("BlockingMethodInNonBlockingContext")//We're in a coroutine
            val bitmap = futureTarget?.get()

            onLoaded.invoke(bitmap)
        }
    }
}