package oppen.light.views

import android.animation.ValueAnimator
import android.content.Context
import android.content.res.Resources
import android.graphics.*
import android.util.AttributeSet
import android.util.TypedValue
import android.view.GestureDetector
import android.view.HapticFeedbackConstants
import android.view.MotionEvent
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.annotation.ColorInt
import androidx.core.animation.doOnEnd
import oppen.light.R
import kotlin.math.absoluteValue

class InertialSlider @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0):
    View(context, attrs, defStyleAttr), GestureDetector.OnGestureListener {

    private val gestureDetector = GestureDetector(getContext(), this)
    private val screenWidth = Resources.getSystem().displayMetrics.widthPixels.toFloat()
    private val rulerWidth = screenWidth/2
    private val rulerHeight = dpToPx(10)
    private var xx = screenWidth/2f
    private var value: Int = 0
    private var prevValue = 0

    private var flingAnimator = ValueAnimator()
    private lateinit var ruler: Bitmap

    private val leftGradientPaint = Paint()
    private val rightGradientPaint = Paint()

    private val rulerPaint = Paint().apply {
        style = Paint.Style.STROKE
        strokeWidth = dpToPx(1)
        color = Color.WHITE
        isAntiAlias = false
    }

    private val valuePaint = Paint().apply {
        style = Paint.Style.FILL
        color = Color.WHITE
        textSize = dpToPx(12)
        isAntiAlias = false
    }

    private val sourceRect = Rect()
    private val destRect = Rect()

    @ColorInt private var colorSurface: Int = 0

    /**
     *
     */
    private var onChange: (value: Int) -> Unit = {_ ->}
    private var debounceInterval = 100
    private var lastChangeEmit = 0L

    init {
        generateRuler()

        val typedValue = TypedValue()
        context.theme.resolveAttribute(R.attr.colorSurface, typedValue, true)
        colorSurface = typedValue.data
        colorSurface = Color.BLACK
    }

    fun setValueListener(debounceInterval: Int, onChange: (value: Int) -> Unit){
        this.debounceInterval = debounceInterval
        this.onChange = onChange
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        leftGradientPaint.shader = LinearGradient(0f, 0f, width.toFloat(), 0f, colorSurface, Color.TRANSPARENT, Shader.TileMode.MIRROR)
        rightGradientPaint.shader = LinearGradient(width.toFloat(), 0f, 0f, 0f, colorSurface, Color.TRANSPARENT, Shader.TileMode.MIRROR)
    }

    private fun generateRuler(){
        val units = 20
        ruler = Bitmap.createBitmap(rulerWidth.toInt(), rulerHeight.toInt(), Bitmap.Config.ARGB_8888)
        val unit = rulerWidth/units
        val canvas = Canvas(ruler)
        var x = 0f
        repeat(units + 1){
            canvas.drawLine(x, 0f, x, rulerHeight, rulerPaint)
            x += unit
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return when {
            gestureDetector.onTouchEvent(event) -> true
            else -> super.onTouchEvent(event)
        }
    }

    override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {

        xx -= distanceX

        //Boundaries
        when {
            xx < 0f -> xx = 0f
            xx + rulerWidth > screenWidth -> xx = screenWidth/2f
        }

        setValue()
        invalidate()
        emitChange()
        return true
    }

    fun setValue(){
        prevValue = value

        value = map(xx, 0f, screenWidth / 2f, 100f, 0f).toInt()

        if(prevValue != value && (value == 100 || value == 0) && isHapticFeedbackEnabled){
            performHapticFeedback(HapticFeedbackConstants.GESTURE_END)
        }
    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {

        val velocity = velocityX/50

        when {
            velocity < 0 -> flingAnimator = ValueAnimator.ofFloat(xx + velocity.absoluteValue, xx)
            velocity > 0 -> flingAnimator = ValueAnimator.ofFloat(xx - velocity.absoluteValue, xx)
        }

        flingAnimator.interpolator = DecelerateInterpolator()
        flingAnimator.duration = 400
        flingAnimator.addUpdateListener { animator ->
            xx = animator.animatedValue as Float
            setValue()
            invalidate()
        }
        flingAnimator.doOnEnd {
            emitChange()
        }
        flingAnimator.start()

        return true
    }

    private fun emitChange(){
        val time = System.currentTimeMillis()
        if (time - lastChangeEmit >= debounceInterval) {
            lastChangeEmit = time
            onChange(value)
        }
    }

    private val bluePaint = Paint().apply {
        style = Paint.Style.FILL
        isAntiAlias = false
        colorFilter =  PorterDuffColorFilter(Color.parseColor("#ff8888"), PorterDuff.Mode.MULTIPLY)
    }

    override fun draw(canvas: Canvas?) {
        super.draw(canvas)

        canvas?.drawBitmap(ruler, xx, height - rulerHeight, rulerPaint)

        sourceRect.set(0, 0, ((screenWidth/2) - xx).toInt(), rulerHeight.toInt())
        destRect.set(xx.toInt(), (height - rulerHeight).toInt(),  (screenWidth/2).toInt(), height)
        canvas?.drawBitmap(ruler, sourceRect, destRect, bluePaint)

        canvas?.drawRect(0f, 0f, width / 2f, height.toFloat(), leftGradientPaint)
        canvas?.drawRect(width / 2f, 0f, width.toFloat(), height.toFloat(), rightGradientPaint)

        canvas?.drawLine(width / 2f, height.toFloat(), width / 2f, height / 2f, rulerPaint)

        canvas?.drawText(
            "$value",
            screenWidth / 2f - valuePaint.measureText("$value") / 2,
            valuePaint.textSize,
            valuePaint
        )
    }

    override fun onDown(e: MotionEvent?): Boolean = true
    override fun onSingleTapUp(e: MotionEvent?): Boolean = true
    override fun onShowPress(e: MotionEvent?) = Unit
    override fun onLongPress(e: MotionEvent?) = Unit

    private fun dpToPx(dp: Int): Float {
        return (dp.toFloat() * Resources.getSystem().displayMetrics.density)
    }

    private fun map(inValue: Float, inStart: Float, inEnd: Float, outStart: Float, outEnd: Float): Float {
        return outStart + (outEnd - outStart) * ((inValue - inStart) / (inEnd - inStart))
    }
}