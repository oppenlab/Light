package oppen.light.options

data class Option(val titleRes: Int, val iconRes: Int)