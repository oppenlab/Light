package oppen.light.options

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import oppen.light.R
import oppen.light.databinding.OptionBinding

class OptionRecyclerAdapter: RecyclerView.Adapter<OptionRecyclerAdapter.ViewHolder>() {

    class ViewHolder(val binding: OptionBinding) : RecyclerView.ViewHolder(binding.root)

    private val options = mutableListOf<Option>()

    private var selectedIndex = 0

    fun updateOptions(options: List<Option>) {
        this.options.clear()
        this.options.addAll(options)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = OptionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val option = options[position]

        holder.binding.optionLabel.setText(option.titleRes)
        holder.binding.optionButton.setImageResource(option.iconRes)

        if(selectedIndex == position){
            holder.binding.optionButton.background = holder.itemView.context.getDrawable(R.drawable.option_selected)
        }else{
            holder.binding.optionButton.background = holder.itemView.context.getDrawable(R.drawable.option_unselected)
        }
    }

    override fun getItemCount(): Int = options.size
}