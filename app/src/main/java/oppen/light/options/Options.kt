package oppen.light.options

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import oppen.light.R

class OptionsView(private val context: Context) {

    private val adapter = OptionRecyclerAdapter()

    init {
        val options = mutableListOf<Option>()
        options.add(Option(R.string.option_brightness, R.drawable.toggle_brightness))
        options.add(Option(R.string.option_contrast, R.drawable.toggle_contrast))
        options.add(Option(R.string.option_shadows, R.drawable.toggle_shadows))
        options.add(Option(R.string.option_saturation, R.drawable.toggle_saturation))
        options.add(Option(R.string.option_exposure, R.drawable.toggle_exposure))
        options.add(Option(R.string.option_grain, R.drawable.toggle_grain))
        options.add(Option(R.string.option_vignette, R.drawable.toggle_vignette))
        adapter.updateOptions(options)
    }

    fun configureOptionsRecycler(
        recycler: RecyclerView,
        onOption: (option: Option) -> Unit
    ): OptionsView{
        recycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recycler.adapter = adapter
        return this
    }
}