package oppen.light.controls

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import oppen.light.R
import oppen.light.databinding.ControlBinding

class ControlsRecyclerAdapter: RecyclerView.Adapter<ControlsRecyclerAdapter.ViewHolder>() {

    class ViewHolder(val binding: ControlBinding) : RecyclerView.ViewHolder(binding.root)

    private val controls = mutableListOf<Control>()

    private var selectedIndex = 0

    fun updateControls(controls: List<Control>) {
        this.controls.clear()
        this.controls.addAll(controls)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ControlBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val option = controls[position]

        holder.binding.controlLabel.setText(option.titleRes)

        if(selectedIndex == position){
            holder.binding.controlLayout.background = holder.itemView.context.getDrawable(R.drawable.control_selected)
        }else{
            holder.binding.controlLayout.background = holder.itemView.context.getDrawable(R.drawable.control_unselected)
        }
    }

    override fun getItemCount(): Int = controls.size

    fun getControl(position: Int): Control = controls[position]

    fun setSelected(position: Int) {
        val oldSelectedIndex = selectedIndex
        selectedIndex = position
        notifyItemChanged(oldSelectedIndex)
        notifyItemChanged(selectedIndex)
    }
}