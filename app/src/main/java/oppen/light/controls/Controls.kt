package oppen.light.controls

import android.content.Context
import android.graphics.Paint
import android.graphics.Typeface
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import oppen.light.R

class Controls {

    fun configureControlsRecycler(
        context: Context,
        recycler: RecyclerView,
        controls: List<Control>,
        onChange: (control: Control) -> Unit): Controls {

        recycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        if(recycler.itemDecorationCount == 1) recycler.removeItemDecorationAt(0)

        //Ensures selected option is in the middle of the screen
        val firstCellWidth = getChipWidth(context, context.getString(controls.first().titleRes))
        val lastCellWidth = getChipWidth(context, context.getString(controls.last().titleRes))
        val margin = context.resources.getDimensionPixelSize(R.dimen.chip_horizontal_margin)
        recycler.addItemDecoration(OffsetItemDecoration(firstCellWidth, lastCellWidth, margin))

        recycler.onFlingListener = null

        val snapHelper = PagerSnapHelper()

        snapHelper.attachToRecyclerView(recycler)

        val adapter = ControlsRecyclerAdapter()

        val indexChangeListener = IndexChangeListener(snapHelper){ position ->
            adapter.setSelected(position)
            onChange(adapter.getControl(position))
        }

        recycler.clearOnScrollListeners()
        recycler.addOnScrollListener(indexChangeListener)

        recycler.swapAdapter(adapter, true)

        adapter.updateControls(controls)

        return this
    }

    private fun getChipWidth(context: Context, label: String): Int{
        val paint = Paint()
        paint.typeface = Typeface.defaultFromStyle(Typeface.NORMAL)
        paint.textSize = context.resources.getDimensionPixelSize(R.dimen.chip_text_size).toFloat()
        val textWidth = paint.measureText(label)
        val padding = context.resources.getDimensionPixelSize(R.dimen.chip_horizontal_padding) * 2
        return (textWidth + padding).toInt()
    }
}