package oppen.light.controls

import oppen.light.R

sealed class Control(val titleRes: Int){
  class AdjustControl: Control(R.string.adjust)
  class ColourControl: Control(R.string.colour)
  class MonochromeControl: Control(R.string.monochrome)
  class BrandControl(brandRes: Int): Control(brandRes)

  companion object{
    fun getDefaultSet(): List<Control>{
      return listOf(
        AdjustControl(),
        ColourControl(),
        MonochromeControl()
      )
    }

    fun getColourSet(): List<Control>{
      return listOf(
        BrandControl(R.string.brand_agfa),
        BrandControl(R.string.brand_fuji),
        BrandControl(R.string.brand_kodak),
        BrandControl(R.string.brand_polaroid)
      )
    }

    fun getMonochromeSet(): List<Control>{
      return listOf(
        BrandControl(R.string.brand_agfa),
        BrandControl(R.string.brand_fuji),
        BrandControl(R.string.brand_ilford),
        BrandControl(R.string.brand_kodak),
        BrandControl(R.string.brand_polaroid),
        BrandControl(R.string.brand_rollei)
      )
    }
  }
}