package oppen.light.controls

import android.content.res.Resources
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

class OffsetItemDecoration(
    private val firstItemWidth: Int,
    private val lastItemWidth: Int,
    private val margin: Int) : ItemDecoration() {

    private val screenWidth = Resources.getSystem().displayMetrics.widthPixels

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        val layoutParams = view.layoutParams as MarginLayoutParams
        layoutParams.leftMargin = 0
        layoutParams.rightMargin = 0

        when {
            parent.getChildAdapterPosition(view) == 0 -> {
                val offset = (screenWidth / 2f) - ((firstItemWidth/2))
                outRect.left = offset.toInt()
                outRect.right = margin
            }
            parent.getChildAdapterPosition(view) == state.itemCount - 1 -> {
                val offset = (screenWidth / 2f) - ((lastItemWidth/2))
                outRect.left = margin
                outRect.right = offset.toInt()
            }
            else ->{
                outRect.left = margin
                outRect.right = margin
            }
        }
    }
}