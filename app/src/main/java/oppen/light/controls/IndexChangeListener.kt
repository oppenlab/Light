package oppen.light.controls


import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper

class IndexChangeListener(private val snapHelper: SnapHelper, var onChange: (position: Int) -> Unit) : RecyclerView.OnScrollListener() {

    private var snapPosition = RecyclerView.NO_POSITION
    private var velocity = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        velocity = dx
    }

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        when (newState) {
            RecyclerView.SCROLL_STATE_IDLE -> {
                val position = snapHelper.findTargetSnapPosition(recyclerView.layoutManager, velocity, 0)
                when {
                    this.snapPosition != position -> {
                        this.snapPosition = position
                        onChange(position)
                    }
                }
            }
        }
    }
}