package oppen.light

import android.app.Application
import android.renderscript.RenderScript
import oppen.light.lib.RenderScriptProvider

class Light: Application() {

    override fun onCreate() {
        super.onCreate()
        RenderScriptProvider.initialise(RenderScript.create(this))
    }
}