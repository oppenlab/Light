package oppen.light.ui.editor

import android.graphics.Bitmap
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.lifecycle.ViewModel
import oppen.light.ImageIO
import oppen.light.SingleLiveEvent

class EditorViewModel: ViewModel() {

  lateinit var imageIO: ImageIO
  private var sourceUri: Uri? = null
  private var previewUri: Uri? = null
  private var thumbnailUri: Uri? = null
  var thumbnail: Bitmap? = null

  private val uiAction: SingleLiveEvent<UIAction?> = SingleLiveEvent()
  private fun onUIAction(action: UIAction?) {
    uiAction.value = action
  }

  fun initialise(imageIO: ImageIO): SingleLiveEvent<UIAction?> {
    this.imageIO = imageIO
    return uiAction
  }

  fun open(){
    onUIAction(UIAction.OpenAction)
  }

  fun loadImage(sourceUri: Uri, onPreview: (previewUri: Uri) -> Unit) {
    this.sourceUri = sourceUri
      imageIO.loadImage(sourceUri){ previewUri, thumbnailUri ->
          this.previewUri = previewUri
          this.thumbnailUri = thumbnailUri

        imageIO.get(thumbnailUri){ thumbnail ->
          this.thumbnail = thumbnail
          Handler(Looper.getMainLooper()).post {
            onPreview(previewUri)
          }
        }
      }
  }
}