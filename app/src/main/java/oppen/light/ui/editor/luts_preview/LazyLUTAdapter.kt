package oppen.light.ui.editor.luts_preview

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.renderscript.RenderScript
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.collection.LruCache
import androidx.recyclerview.widget.RecyclerView
import oppen.light.R
import oppen.light.databinding.FilterThumbnailBinding
import oppen.light.lib.luts.LUT
import oppen.light.lib.luts.LUTFlow

class LazyLUTAdapter (
  val context: Context,
  val thumbnail: Bitmap?,
  private val luts: List<Pair<String, Int>>,
  private val onClick: (lut: Pair<String, Int>, direction: Int, count: Int) -> Unit): RecyclerView.Adapter<LazyLUTAdapter.FilterViewHolder>() {

  class FilterViewHolder(val binding: FilterThumbnailBinding) : RecyclerView.ViewHolder(binding.root)

  private val cacheSize = 6 * 1024 * 1024//6MiB
  private val bitmapCache = LruCache<String, Bitmap>(cacheSize)

  private val handler = Handler(Looper.getMainLooper())


  init {
    bitmapCache.evictAll()//todo - this may not be necessary
    val flowLuts = mutableListOf<LUT>()
    luts.forEachIndexed { index, lut ->
      flowLuts.add(LUT(lut.first, lut.second, index))
    }
    LUTFlow(context).process(thumbnail!!, flowLuts){ lutReady ->
      handler.post {
        bitmapCache.put(lutReady.first.name, lutReady.second)
        notifyItemChanged(lutReady.first.adapterPosition)
      }
    }
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterViewHolder { val view = LayoutInflater.from(parent.context).inflate(R.layout.filter_thumbnail, null)
    val binding = FilterThumbnailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    return FilterViewHolder(binding)
  }

  override fun getItemCount(): Int = luts.size

  override fun onBindViewHolder(holder: FilterViewHolder, position: Int) {
    val lut = luts[position]

    holder.binding.adapter = this
    holder.binding.lut = lut

    val label = lut.first
    holder.binding.thumbnailLabel.text = label
    holder.binding.thumbnailImage.setImageBitmap(null)
    bitmapCache.get(lut.first)?.run {
      holder.binding.thumbnailImage.setImageBitmap(this)
    }
  }
}