package oppen.light.ui.editor

sealed class UIAction {
  object OpenAction : UIAction()
}