package oppen.light.ui.editor

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import oppen.light.ImageIO
import oppen.light.R
import oppen.light.controls.Control
import oppen.light.controls.Controls
import oppen.light.databinding.ActivityEditorBinding
import oppen.light.hide
import oppen.light.lib.luts.LUTResources
import oppen.light.options.*
import oppen.light.show
import oppen.light.ui.editor.luts_preview.LazyLUTAdapter

const val IMAGE_CHOOSER_REQUEST = 0

class EditorActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEditorBinding
    private val model by viewModels<EditorViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditorBinding.inflate(layoutInflater)
        binding.model = model
        val view = binding.root
        setContentView(view)

        model.initialise(ImageIO(this)).observe(this, { action ->
            when(action){
                is UIAction.OpenAction -> {
                    val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
                    intent.addCategory(Intent.CATEGORY_OPENABLE)
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    intent.type = "image/*"
                    startActivityForResult(intent, IMAGE_CHOOSER_REQUEST)
                }
            }
        })

        OptionsView(this).configureOptionsRecycler(binding.optionsRecycler){ option ->
                //show ui for selected option
        }


        binding.lutsRecycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        val lutThumbnailDecoration = DividerItemDecoration(this, LinearLayoutManager.HORIZONTAL)
        ResourcesCompat.getDrawable(resources, R.drawable.thumbnail_divider, null)?.also {
            lutThumbnailDecoration.setDrawable(it)
        }

        binding.lutsRecycler.addItemDecoration(lutThumbnailDecoration)

        Controls().configureControlsRecycler(this, binding.controlsRecycler, Control.getDefaultSet()){ control ->
            when(control){
                is Control.AdjustControl -> {
                    println("Top level control change: show adjust")
                    showView(binding.adjustLayout)
                    binding.lutsLayout.hide()
                }
                is Control.ColourControl -> {
                    println("Top level control change: show colour films")
                    loadColourFilms()
                }
                is Control.MonochromeControl -> {
                    println("Top level control change: show monochrome films")
                    loadMonochromeFilms()
                }
                else -> {
                    println("Top level control change: unknown")
                }
            }
        }

        binding.inertialSlider.setValueListener(250) { value ->
            println("inertial slider value: $value")
        }
    }

    private fun loadMonochromeFilms() {
        println("loadMonochromeFilms()")
        val monochromeLuts = Control.getMonochromeSet()
        Controls().configureControlsRecycler(this, binding.lutsBrandRecycler, monochromeLuts) { monochromeBrand ->
            applyMonochromeLuts(monochromeBrand)
        }
        showView(binding.lutsLayout)
        binding.adjustLayout.hide()

        applyMonochromeLuts(monochromeLuts.first())
    }

    private fun applyMonochromeLuts(monochromeBrand: Control) {
        val brand = resources.getString(monochromeBrand.titleRes)
        println("Monochrome control change: load brand: $brand")
        val adapter = LazyLUTAdapter(
            this,
            model.thumbnail,
            LUTResources.getMonochromeLuts(this, brand)
        ) { lut: Pair<String, Int>, direction: Int, count: Int ->
        }
        binding.lutsRecycler.swapAdapter(adapter, true)
    }

    private fun loadColourFilms() {
        println("loadColourFilms()")
        val colourLuts = Control.getColourSet()
        Controls().configureControlsRecycler(this, binding.lutsBrandRecycler, colourLuts) { colourBrand ->
            applyColourLuts(colourBrand)
        }
        showView(binding.lutsLayout)
        binding.adjustLayout.hide()

        applyColourLuts(colourLuts.first())
    }

    private fun applyColourLuts(colourBrand: Control) {
        val brand = resources.getString(colourBrand.titleRes)
        println("Colour control change: load brand: $brand")
        val adapter = LazyLUTAdapter(
            this,
            model.thumbnail,
            LUTResources.getColourLuts(this, brand)
        ) { lut: Pair<String, Int>, direction: Int, count: Int ->
        }
        binding.lutsRecycler.swapAdapter(adapter, true)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == IMAGE_CHOOSER_REQUEST && resultCode == RESULT_OK){
            data?.data?.run {
                model.loadImage(this) { previewUri ->
                    binding.emptyLayout.hide()
                    binding.previewImageView.load(previewUri)
                    if(!binding.controls.isVisible)showView (binding.controls)//temp
                }
            }
        }else{
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun showView(view: View){
        view.show()
        view.alpha = 0f
        view.animate().alpha(1.0f).duration = 1000
    }
}