# Light

** Note - this is under early development and is non-functional. **

A follow-up/sequel to [Lätt](https://codeberg.org/oppenlab/Latt/). Lätt produces fantastic images (I still use it daily) but the user experience never really got as polished as it needed to be for a production app. The code was written without any thought given to architecture and there's a fair amount of debt. In addition to this Google recently released an improved Google Photos editing experience which showcases some much more fluid ways of editing photos on a mobile device. This project aims to both produce more maintainable code, and also create a much simpler UX than Lätt.

## Architecture

Any and all image processing will take place in the lib module. The app module will contain... the app. 

### App module

At least aim for something MVVM-like, and use Android's (relatively) new ViewModel, and View and Data binding API.

### Lib module

Lätt had a `PhotoProcessor` [God-object](https://en.wikipedia.org/wiki/God_object), only 200 lines but still it did far too much. Light has a very simple `ImageProcessor` which takes a Bitmap and list of `Filter` implementations. Each filter implementation can fully encapsulate a single image processing technique: applying a LUT, contrast, brightness, or even some 1bit dithering techniques. This hopefully means that while any individual process can be a debt-ridden mess the overall ['cyclomatic complexity'](https://en.wikipedia.org/wiki/Cyclomatic_complexity) can be kept low. 

I couldn't resist adding the ImageProcessor to a Bitmap extension function:

```
aBitmap.process(filters){ result ->
    //...
}
```